A Pen created at CodePen.io. You can find this one at http://codepen.io/jlalovi/pen/qvknb.

 Hello people! I have started recently to study HTML5 and CSS3 and this is my first project! :)

Freebie PSD: Flat UI Kit 2 (Blog) is a design created by Riki Tanone in this link:

- http://drbl.in/gUhL

I have used also some image fonts from:
- http://weloveiconfonts.com/
- http://fontawesome.io/

And the profile images from:
- http://lorempixel.com/

Thank you for your support!