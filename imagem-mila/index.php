 <!--  Smoothy Template  - http://www.templatemo.com/preview/templatemo_396_smoothy  -->

<?php include("mostra-alerta.php"); ?>
<!DOCTYPE html>
<html>
  <head>
    <title>Imagem Comunicação e Marketing</title>
    <meta name="keywords" content="" />
	  <meta name="description" content="" />
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

	  <!-- Favicon -->
    <link rel="shortcut icon" href="images/favicon2.ico">

    <!-- Bootstrap -->
	  <link rel="stylesheet" href="css/reset.css">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/templatemo_style.css" rel="stylesheet">

    <link href="css/circle.css" rel="stylesheet">
    <link href="css/jquery.bxslider.css" rel="stylesheet" />
    <link rel="stylesheet" href="css/nivo-slider.css">
    <link rel="stylesheet" href="css/slimbox2.css" type="text/css" media="screen" />

    <!-- Resource style -->
    <link rel="stylesheet" href="css/style.css">

    <!-- Modernizr -->
    <script src="js/modernizr.js"></script>
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/JavaScript" src="js/slimbox2.js"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>

    <link rel="stylesheet" type="text/css" href="css/ddsmoothmenu.css" />
	  <!-- <script type="text/javascript" src="js/ddsmoothmenu.js"></script> -->

    <!-- Script do formulário de contato  -->
    <!-- <script type="text/javascript" src="js/reset.js"></script> -->
    <script type="text/javascript" src="js/alertify.min.js"></script>
    <link href="css/alertify.core.css" rel="stylesheet">
    <link href="css/alertify.default.css" rel="stylesheet">

    <!-- Font -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,100,600' rel='stylesheet' type='text/css'>
    <style media="screen">
        .alert-danger {
            text-align: center;
        }
    </style>
  </head>

  <body onload="slide();" id="voltarTopo">
      <?php mostraAlerta("success"); ?>
      <?php mostraAlerta("danger"); ?>
    <!-- Header - Inicio -->
    <header>
      <!-- Menu - Inicio -->
      <div id="templatemo_home">
      	<div class="templatemo_top">
          <div class="container templatemo_container">
            <div class="row">
              <div class="col-sm-3 col-md-3" >
                <div class="logo">
                  <a href="#"><img src="images/templatemo_logo.png" alt="smoothy html5 template"></a>
                </div>
              </div>
              <div class="col-sm-9 col-md-9 templatemo_col9">
            	 <div id="top-menu">
                  <nav class="mainMenu">
                    <ul class="nav">
                      <li><a class="menu" href="#templatemo_home">Home</a></li>
                      <li><a class="menu" href="#templatemo_reason">Agência</a></li>
                      <li><a class="menu" href="#templatemo_portfolio">Serviços</a></li>
  				            <!-- <li><a class="menu" href="#templatemo_blog">Portfólio</a></li> -->
                      <li><a class="menu" href="#templatemo_partners">Clientes</a></li>
                      <li><a class="menu" href="#templatemo_contact">Contato</a></li>
                    </ul>
                  </nav>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="clear"></div>
      <div class="">
          <?php mostraAlerta("success"); ?>
      </div>
      <!-- Menu - Fim -->

      <!-- Slider de Banner -->
      <div  id="slider"  class="nivoSlider templatemo_slider">
        <a href="#"><img src="images/slider/banner1.jpg" alt="Banner"></a>
        <a href="#"><img src="images/slider/banner2.jpg" alt="Banner"></a>
      </div>
    </header>
    <!-- Header - Fim -->

    <!-- Quadradinhos com Serviços - Inicio -->
  	<div class="templatemo_lightgrey_about" id="templatemo_about">
	   <div class="container">

        <div class="col-xs-6 col-sm-6 col-md-3 templatemo_col12">
        	<div class="item project-post">
						<div class="templatemo_about_box">
              <div class="square_coner">
                <span class="texts-a"><i class="fa fa-newspaper-o"></i></span>
              </div>
              Assessoria de Imprensa
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3 hover-box" >
              <div class="inner-hover-box">
                <ul class="lista_hover">
						      <li>&#8226; Divulgação para sites, blogs, jornais, revistas e rádios</li>
                  <li>&#8226; Lançamento de produtos</li>
                  <li>&#8226; Coletivas de imprensa</li>
                  <li>&#8226; Planejamento estratégico de divulgação</li>
                  <li>&#8226; Media training</li>
						    </ul>
              </div>
            </div>
					</div>
        </div>

        <div class="col-xs-6 col-sm-6 col-md-3 templatemo_col12">
          <div class="item project-post">
            <div class="templatemo_about_box">
              <div class="square_coner">
                <span class="texts-a"><i class="fa fa-globe"></i></span>
              </div>
              Soluções para Divulgação
            </div>
            <div class="col-xs-6 col-sm-6 col-md-3 hover-box" >
              <div class="inner-hover-box">
						    <ul class="lista_hover">
						      <li>&#8226; Criação e manutenção de sites, redes sociais e boletins eletrônicos</li>
                  <li>&#8226; Criação de Logotipos</li>
                  <li>&#8226; Produção de material gráfico para divulgação</li>
                  <li>&#8226; Revisão de textos em geral e transcrição de entrevistas</li>
						    </ul>
              </div>
            </div>
          </div>
        </div>

        <div class="col-xs-6 col-sm-6 col-md-3 templatemo_col12 templatemo_margintop10">
          <div class="item project-post">
            <div class="templatemo_about_box">
              <div class="square_coner">
                <span class="texts-a"><i class="fa fa-camera"></i></span>
              </div>
              Cobertura de Eventos
            </div>
            <div class="col-xs-6 col-sm-6 col-md-3 hover-box" >
					   <div class="inner-hover-box">
						  <ul class="lista_hover">
						    <li>&#8226; Oferecemos serviços de Fotografias Profissionais em eventos variados</li>
                <li>&#8226; Disponibilizamos cerimonialistas para a apresentação de eventos na cidade de Santa Rita do Sapucaí e região</li>
						  </ul>
              </div>
              </div>
            </div>
        </div>

        <div class="col-xs-6 col-sm-6 col-md-3 templatemo_col12 templatemo_margintop10">
          <div class="item project-post">
            <div class="templatemo_about_box">
              <div class="square_coner">
                <span class="texts-a"><i class="fa fa-pencil-square-o"></i></span>
              </div>
              Consultoria
            </div>
            <div class="col-xs-6 col-sm-6 col-md-3 hover-box" >
              <div class="inner-hover-box">
						    <ul class="lista_hover">
						      <li>&#8226; Planejamento</li>
                  <li>&#8226; Mapeamento de <br> mensagens-chave</li>
                  <li>&#8226; Definição de políticas de comunicação</li>
                  <li>&#8226; Gestão de crises</li>
                  <li>&#8226; Media training</li>
                  </ul>
              </div>
            </div>
          </div>
        </div>

      </div>
    </div>
    <!-- Quadradinhos com Serviços - Fim -->

    <div class="clear"></div>

    <!-- Por que contratar a Imagem Comunicação? - Inicio -->
    <div class="templatemo_reasonbg" id="templatemo_reason">
	    <div class="container">
        <h2>Por que contratar a Imagem Comunicação?</h2> <br>
        <p>A Imagem Comunicação e Marketing é uma agência focada em Qualidade e Resultado, com larga experiência no planejamento e implementação de Programas de construção, consolidação e reposicionamento da Marca e Imagem da sua Empresa. </p>
        <p>A agência possui um amplo portfólio de serviços para colocar empresas e instituições dos mais diversos setores em sintonia com seus públicos-alvo.</p>
        <p>Nosso diferencial é oferecer aos clientes serviços de qualidade em um único lugar.
  		  Aqui, você encontra todas as soluções integradas de Comunicação desde assessoria de imprensa a desenvolvimento de peças publicitárias, produção de conteúdo e criação de site.</p>
        <p>Conheça melhor os nossos serviços.<br>
        Seja bem-vindo! Você está no lugar certo!
        </p>
      </div>
	  </div>
    <!-- Por que contratar a Imagem Comunicação? - Fim -->

    <div class="clear"></div>

    <!--Nossos Serviços - Inicio-->
    <div class="templatemo_portfolio" id="templatemo_portfolio">
	    <div class="container">
        <h2>Nossos Serviços</h2>
			  <br>
        <p>
          A agência Imagem oferece diversas soluções de Comunicação e Marketing direcionadas a empresas que almejam projetar sua marca, <br> principalmente na internet e aumentar seu faturamento a partir desta mídia.<br>
          Conheça nossas soluções e entre em contato.<br> Um consultor da nossa Agência irá analisar suas necessidades e determinar as melhores soluções para o seu negócio de acordo com seu público-alvo.
    			<div class="lista">
            <ul>
              <li>Assessoria de imprensa </li>
              <li>Cobertura de eventos </li>
              <li>Ensaios Fotográficos </li>
              <li>Criação e manutenção de sites, blogs e perfis em redes sociais</li>
              <li>Criação de slides e apresentação de eventos</li>
              <li>Revisão de textos em geral e padronização ABNT</li>
    			    <li>Transcrição de entrevistas</li>
              <li>Produção de material gráfico para divulgação</li>
              <li>Gerenciamento de crise</li>
              <li>Criação de logotipos</li>
              <li>Consultoria</li>
              </ul>
          </div>
    			<br>
    			Trabalhamos com uma equipe de especialistas independentes, que podem ser acionados on demand, <br> de acordo com a necessidade do cliente. <br> Desta forma, prestamos serviços de Comunicação de maneira transparente e sob medida para sua realidade!
			  </p>
      </div>
	  </div>
    <!--Nossos Serviços - Fim-->

    <div class="clear"></div>

    <!--Porfolio - Inicio-->
   <!--  <div class="templatemo_blog" id="templatemo_blog">
    	<h2>Portfólio</h2>
			<br>
        <div class="clear"></div>
        <div class="container">
		<div class="col-xs-6 col-sm-6 col-md-3 templatemo_col12">
              	<div class="portfolio-item">
							<div class="portfolio-thumb">
								<img src="images/portfolio/1.jpg" alt="portfolio 1">
								<div class="overlay-p">
									<a href="images/portfolio/1.jpg" data-rel="lightbox[portfolio]">
                                    	<ul>
                                        	<li>Blue Sky</li>
                                            <li class="fa fa-circle"></li>
                                            <li class="fa fa-circle fsmall"></li>
                                            <li class="fa fa-circle"></li>
                                            <li class="fa fa-circle fsmall"></li>
                                            <li class="fa fa-search fa-2x"></li>
                                        </ul>
									</a>
								</div>
							</div>
						</div>

              </div>
              <div class="col-xs-6 col-sm-6 col-md-3 templatemo_col12">
              	<div class="portfolio-item">
							<div class="portfolio-thumb">
								<img src="images/portfolio/2.jpg" alt="portfolio 2">
								<div class="overlay-p">
									<a href="images/portfolio/2.jpg" data-rel="lightbox[portfolio]">
                                    	<ul>
                                        	<li>Forest</li>
                                            <li class="fa fa-circle"></li>
                                            <li class="fa fa-circle fsmall"></li>
                                            <li class="fa fa-circle"></li>
                                            <li class="fa fa-circle fsmall"></li>
                                            <li class="fa fa-search fa-2x"></li>
                                        </ul>
									</a>
								</div>
							</div>
						</div>

              </div>
              <div class="col-xs-6 col-sm-6 col-md-3 templatemo_col12">
              	<div class="portfolio-item">
							<div class="portfolio-thumb">
								<img src="images/portfolio/3.jpg" alt="portfolio 3">
								<div class="overlay-p">
									<a href="images/portfolio/3.jpg" data-rel="lightbox[portfolio]">
                                    	<ul>
                                        	<li>Forest</li>
                                            <li class="fa fa-circle"></li>
                                            <li class="fa fa-circle fsmall"></li>
                                            <li class="fa fa-circle"></li>
                                            <li class="fa fa-circle fsmall"></li>
                                            <li class="fa fa-search fa-2x"></li>
                                        </ul>
									</a>
								</div>
							</div>
						</div>

              </div>
              <div class="col-xs-6 col-sm-6 col-md-3 templatemo_col12">
              	<div class="portfolio-item">
							<div class="portfolio-thumb">
								<img src="images/portfolio/4.jpg" alt="portfolio 4">
								<div class="overlay-p">
									<a href="images/portfolio/4.jpg" data-rel="lightbox[portfolio]">
                                    	<ul>
                                        	<li>Black and White</li>
                                            <li class="fa fa-circle"></li>
                                            <li class="fa fa-circle fsmall"></li>
                                            <li class="fa fa-circle"></li>
                                            <li class="fa fa-circle fsmall"></li>
                                            <li class="fa fa-search fa-2x"></li>
                                        </ul>
									</a>
								</div>
							</div>
						</div>

              </div>
             </div>
               <div class="clear"></div>
              <div class="container">
              		<div class="col-xs-6 col-sm-6 col-md-3 templatemo_col12">
              	<div class="portfolio-item">
							<div class="portfolio-thumb">
								<img src="images/portfolio/5.jpg" alt="portfolio 5">
								<div class="overlay-p">
									<a href="images/portfolio/5.jpg" data-rel="lightbox[portfolio]">
                                    	<ul>
                                        	<li>Rotary</li>
                                            <li class="fa fa-circle"></li>
                                            <li class="fa fa-circle fsmall"></li>
                                            <li class="fa fa-circle"></li>
                                            <li class="fa fa-circle fsmall"></li>
                                            <li class="fa fa-search fa-2x"></li>
                                        </ul>
									</a>
								</div>
							</div>
						</div>

              </div>
              <div class="col-xs-6 col-sm-6 col-md-3 templatemo_col12">
              	<div class="portfolio-item">
							<div class="portfolio-thumb">
								<img src="images/portfolio/6.jpg" alt="portfolio 6">
								<div class="overlay-p">
									<a href="images/portfolio/6.jpg" data-rel="lightbox[portfolio]">
                                    	<ul>
                                        	<li>Rotary</li>
                                            <li class="fa fa-circle"></li>
                                            <li class="fa fa-circle fsmall"></li>
                                            <li class="fa fa-circle"></li>
                                            <li class="fa fa-circle fsmall"></li>
                                            <li class="fa fa-search fa-2x"></li>
                                        </ul>
									</a>
								</div>
							</div>
						</div>

              </div>
              <div class="col-xs-6 col-sm-6 col-md-3 templatemo_col12">
              	<div class="portfolio-item">
							<div class="portfolio-thumb">
								<img src="images/portfolio/7.jpg" alt="portfolio 7">
								<div class="overlay-p">
									<a href="images/portfolio/7.jpg" data-rel="lightbox[portfolio]">
                                    	<ul>
                                        	<li>Blue Sky</li>
                                            <li class="fa fa-circle"></li>
                                            <li class="fa fa-circle fsmall"></li>
                                            <li class="fa fa-circle"></li>
                                            <li class="fa fa-circle fsmall"></li>
                                            <li class="fa fa-search fa-2x"></li>
                                        </ul>
									</a>
								</div>
							</div>
						</div>

              </div>
              <div class="col-xs-6 col-sm-6 col-md-3 templatemo_col12">
              	<div class="portfolio-item">
							<div class="portfolio-thumb">
								<img src="images/portfolio/8.jpg" alt="portfolio 8">
								<div class="overlay-p">
									<a href="images/portfolio/8.jpg" data-rel="lightbox[portfolio]">
                                    	<ul>
                                        	<li>Black and White</li>
                                            <li class="fa fa-circle"></li>
                                            <li class="fa fa-circle fsmall"></li>
                                            <li class="fa fa-circle"></li>
                                            <li class="fa fa-circle fsmall"></li>
                                            <li class="fa fa-search fa-2x"></li>
                                        </ul>
									</a>
								</div>
							</div>
						</div>

              </div>
			  <a class="btn btn-large btn-secondary" href="#">Veja Mais</a> -->
       		<!-- <div class="col-xs-6 col-sm-6 col-md-3 templatemo_col12">
            	<div class="templatemo_wrapperblogbox">
                	<a href="#" class="fa fa-calendar tooltip1" title="28 January 2084"></a>
                    <a href="#" class="fa fa-user tooltip2" title="Booker"></a>
                    <a href="#" class="fa fa-tag tooltip3" title="HTML5, CSS3"></a>
                    <a href="#" class="fa fa-comment tooltip4" title="Comments (4)"></a>
                </div>
                <div class="clear"></div>
                <img src="images/templatemo_blogimage01.jpg" alt="blog image 1">
                <div class="clear"></div>
                <div class="templatemo_blogtext">
                	<span class="left">New Touring</span>
                    <span class="right">
                    <a href="#" title="Click more"><i class="fa fa-circle"></i><i class="fa fa-circle"></i><i class="fa fa-circle"></i></a>
                    </span>
                </div>
        	</div>
            <div class="col-xs-6 col-sm-6 col-md-3 templatemo_col12">
            	<div class="templatemo_wrapperblogbox">
                	<a href="#" class="fa fa-calendar tooltip1" title="26 January 2084"></a>
                    <a href="#" class="fa fa-user tooltip2" title="George"></a>
                    <a href="#" class="fa fa-tag tooltip3" title="Template, Design"></a>
                    <a href="#" class="fa fa-comment tooltip4" title="Comments (8)"></a>
                </div>
                <div class="clear"></div>
                <img src="images/templatemo_blogimage02.jpg" alt="blog image 2">
                <div class="clear"></div>
                <div class="templatemo_blogtext">
                	<span class="left">Coffee Shop</span>
                    <span class="right">
                    <a href="#" title="Click more"><i class="fa fa-circle"></i><i class="fa fa-circle"></i><i class="fa fa-circle"></i></a>
                    </span>
                </div>
        	</div>
            <div class="col-xs-6 col-sm-6 col-md-3 templatemo_col12 templatemo_margintop10">
            	<div class="templatemo_wrapperblogbox">
                	<a href="#" class="fa fa-calendar tooltip1" title="24 January 2084"></a>
                    <a href="#" class="fa fa-user tooltip2" title="Jelly Bean"></a>
                    <a href="#" class="fa fa-tag tooltip3" title="Web Design, Portfolio"></a>
                    <a href="#" class="fa fa-comment tooltip4" title="Comments (16)"></a>
                </div>
                <div class="clear"></div>
                <img src="images/templatemo_blogimage03.jpg" alt="blog image 3">
                <div class="clear"></div>
                <div class="templatemo_blogtext">
                	<span class="left">Tea Time</span>
                    <span class="right">
                    <a href="#" title="Click more"><i class="fa fa-circle"></i><i class="fa fa-circle"></i><i class="fa fa-circle"></i></a>
                    </span>
                </div>
        	</div>
            <div class="col-xs-6 col-sm-6 col-md-3 templatemo_col12 templatemo_margintop10">
            	<div class="templatemo_wrapperblogbox">
                	<a href="#" class="fa fa-calendar tooltip1" title="22 January 2084"></a>
                    <a href="#" class="fa fa-user tooltip2" title="John Walker"></a>
                    <a href="#" class="fa fa-tag tooltip3" title="Logo, Creative"></a>
                    <a href="#" class="fa fa-comment tooltip4" title="Comments (32)"></a>
                </div>
                <div class="clear"></div>
                <img src="images/templatemo_blogimage04.jpg" alt="blog image 4">
                <div class="clear"></div>
                <div class="templatemo_blogtext">
                	<span class="left">Mobile First</span>
                    <span class="right">
                    <a href="#" title="Click more"><i class="fa fa-circle"></i><i class="fa fa-circle"></i><i class="fa fa-circle"></i></a>
                    </span>
                </div>
        	</div> -->
<!--         </div>
    </div> -->
   <!--Porfolio - Inicio-->

	<!--Nossos Clientes - Inicio-->
  <div class="section6 templatemo_partner" id="templatemo_partners">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="secHeader">
            <h2 class="text-center">Nossos Clientes</h2>
			      <br><br><br>
          </div>
        </div>
      </div>
    </div>

    <div class="partnerWrap">
      <div  class="slideshow"
            data-cycle-fx=carousel
			      data-cycle-speed=800
            data-cycle-timeout=2000
            data-cycle-carousel-visible=3
            data-cycle-next="#next"
            data-cycle-prev="#prev"
            data-cycle-carousel-fluid=true >

            <!-- Logo dos Clientes -->
            <img alt="Agroeasy"         src="images/partners/agroeasy.jpg" >
            <img alt="HIT"          	src="images/partners/hit.jpg" >
            <img alt="Lumenx"       	src="images/partners/lumenx.jpg" >
            <img alt="Photon Tic"   	src="images/partners/photonTic.jpg" >
            <img alt="Engenharia É" 	src="images/partners/engenhariaE.jpeg" >
            <img alt="Trem"         	src="images/partners/trem.jpg" >
            <img alt="Imidiar"      	src="images/partners/imidiar.jpg" >
            <img alt="GM"           	src="images/partners/gm.jpg" >
            <img alt="Café Jazz"    	src="images/partners/cafeJazz.jpg" >
            <img alt="Radio Top"    	src="images/partners/radioTopMais.jpg" >
			<img alt="Rita Maia"    	src="images/partners/ritinha.jpg" >
			<img alt="TM Importados"    src="images/partners/tmimportados.jpg" >
        </div>
    </div>
  </div>
  <!--Nossos Clientes - Fim-->

    <!-- Depoimento - Inicio -->
    <div class="templatemo_reasonbg cliente">
      <h2>O que dizem nossos clientes?</h2>
        <div class="container">
		      <div class="cd-testimonials-wrapper cd-container">
	          <ul class="cd-testimonials">
  		        <li>
  			        <p>Tive oportunidade de ter a gerente da Imagem como minha colega de trabalho. Criativa, muito profissional, deixou excelentes realizações que foram além do que seu cargo exigia. Será certo seu sucesso, pelo seu conhecimento, simpatia e vontade de fazer acontecer. Sucesso à agência!</p>
  			        <div class="cd-author">
    				      <img src="images/client/mario.jpg" alt="Mario">
    				      <ul class="cd-author-info">
    					      <li>Mario Augusto Nunes</li>
    					      <li>Educação Empreendedora  no Inatel</li>
    				      </ul>
  			        </div>
  		        </li>

  		        <li>
    			      <p>Pela necessidade de adotar estratégias de Comunicação e Marketing para atingir com eficácia os clientes do SPIN - Sistema de Plantações Inteligente, a parceria com a Imagem Comunicação está sendo de extrema importância e nos ajudando a aproximar ainda mais de nossos clientes.</p>
    			      <div class="cd-author">
    				        <img src="images/client/vitor.jpg" alt="Vitor">
    				          <ul class="cd-author-info">
    					         <li>Vitor Ivan D'Angelo</li>
    					         <li>SPIn</li>
    				          </ul>
    			      </div>
  		        </li>

  		        <li>
  			        <p>A Imagem é uma agência de alta performance. Cada profissional com sua sensibilidade particular aliada aos conhecimentos técnicos, culturais e administrativos, resulta numa competência irrefragável. Destaco ainda a criatividade e envolvimento de todos da equipe com cada atividade, fatores evidentes na qualidade e excelência dos produtos e serviços.</p>
  			        <div class="cd-author">
  				        <img src="images/client/henrique.jpg" alt="Henrique">
  				        <ul class="cd-author-info">
  					        <li>Henrique Fernandes</li>
  					        <li>Café com Jazz Trio</li>
  				        </ul>
  			        </div>
  		        </li>

  		        <li class="cd-testimonials-item">
  				      <p>Sou cliente dos profissionais da Imagem desde antes da agência ter esse nome, há mais de 4 anos. Sempre me atenderam da melhor maneira possível e aceitaram todos os desafios a longo e curto prazo. Agência pontual com seus compromissos, além da qualidade nos serviços prestados e estar entre as melhores do mercado.</p>
  				      <div class="cd-author">
  					      <img src="images/client/junior.jpg" alt="Author image">
  					      <ul class="cd-author-info">
  						      <li>Júnior Martins</li>
  						      <li>Homeplay</li>
  					      </ul>
  				      </div> <!-- cd-author -->
  			      </li>
            </ul> <!-- cd-testimonials -->

            <a href="#0" class="cd-see-all">Veja todos</a>
          </div> <!-- cd-testimonials-wrapper -->

          <div class="cd-testimonials-all">
	          <div class="cd-testimonials-all-wrapper">
		        <ul>
			        <li class="cd-testimonials-item">
  				      <p>"Tive oportunidade de ter a gerente da Imagem como minha colega de trabalho. Criativa, muito profissional, deixou excelentes realizações que foram além do que seu cargo exigia. Será certo seu sucesso, pelo seu conhecimento, simpatia e vontade de fazer acontecer. Sucesso à agência".</p>
  				      <div class="cd-author">
  					      <img src="images/client/mario.jpg" alt="Mario">
  					      <ul class="cd-author-info">
  						      <li>Mario Augusto Nunes</li>
  						      <li>Educação Empreendedora  no Inatel</li>
  					      </ul>
  				      </div> <!-- cd-author -->
			        </li>

			    <li class="cd-testimonials-item">
				    <p>"Pela necessidade de adotar estratégias de Comunicação e Marketing para atingir com eficácia os clientes do SPIN - Sistema de Plantações Inteligente, a parceria com a Imagem Comunicação está sendo de extrema importância e nos ajudando a aproximar ainda mais de nossos clientes".</p>
			        <div class="cd-author">
				        <img src="images/client/vitor.jpg" alt="Vitor">
				        <ul class="cd-author-info">
					        <li>Vitor Ivan D'Angelo</li>
					        <li>SPIn</li>
				        </ul>
				      </div> <!-- cd-author -->
			    </li>

			    <li class="cd-testimonials-item">
				    <p>"A Imagem é uma agência de alta performance. Cada profissional com sua sensibilidade particular aliada aos conhecimentos técnicos, culturais e administrativos, resulta numa competência irrefragável. Destaco ainda a criatividade e envolvimento de todos da equipe com cada atividade, fatores evidentes na qualidade e excelência dos produtos e serviços".</p>
			        <div class="cd-author">
				        <img src="images/client/henrique.jpg" alt="Author image">
				        <ul class="cd-author-info">
					        <li>Henrique Fernandes</li>
					        <li>Café com Jazz Trio</li>
				        </ul>
			        </div> <!-- cd-author -->
			    </li>

			    <li class="cd-testimonials-item">
				    <p>"Sou cliente dos profissionais da Imagem desde antes da agência ter esse nome, há mais de 4 anos. Sempre me atenderam da melhor maneira possível e aceitaram todos os desafios a longo e curto prazo. Agência pontual com seus compromissos, além da qualidade nos serviços prestados e estar entre as melhores do mercado".</p>
				    <div class="cd-author">
					    <img src="images/client/junior.jpg" alt="Author image">
					    <ul class="cd-author-info">
						    <li>Júnior Martins</li>
						    <li>Homeplay</li>
					    </ul>
				    </div> <!-- cd-author -->
			    </li>
		    </ul>
	    </div>	<!-- cd-testimonials-all-wrapper -->

	    <a href="#0" class="close-btn">Close</a>
    </div> <!-- cd-testimonials-all -->
		 <!-- Depoimento - Fim -->

    <div class="clear"></div>
    </div>
	 </div>

     <div class="clear"></div>
    <!--Our Client End-->

    <!--Contato - Inicio -->
    <div class="templatemo_lightgrey" id="templatemo_contact">
    <div class="templatemo_paracenter">
      <h2>Fale Conosco</h2><br>
    </div>
    <div class="clear"></div>
        <div class="container">
          <div class="ajustaForm">
            <div class="ajustaFormInner">
              <form role="form" action="envia-contato.php" id="meuFormulario" method="post">
                <p id="returnmessage"></p>
                <div class="row">
                  <div class="col-md-5">
                    <div class="form-group">
                      <input class="form-control" type="text" placeholder="Nome" name="nome" required id="name" />
                    </div>
                    <div class="form-group">
                      <input name="email" type="text" class="form-control" id="email" placeholder="Email" maxlength="30">
                    </div>
                    <div class="form-group">
                      <input class="form-control" type="text" placeholder="Telefone" id="contact" name="telefone" required id="contact" />
                    </div>
                  </div>
                  <div class="col-md-7">
                    <div class="form-group">
                      <textarea rows="12" placeholder="Mensagem" class="form-control" name="mensagem" id="message" ></textarea>
                    </div>
                  </div>
                </div>
                <div>
                  <input type="submit" class="btn btn-secundary" id="submit" value="Enviar" style="float:right; margin-top:0px;"/>
                  <!-- <button lass="btn btn-secundary" id="submit" style="float:right; margin-top:0px;">Enviar</button> -->
                </div>
              </form>
            </div>
          </div>
        </div>
    </div>
    <!--Contato - Fim -->

    <!--Radapé - Inicio-->
    <div class="templatemo_footer">
    	<div class="container">
        <div class="col-xs-6 col-sm-6 col-md-3 templatemo_col12">
          <h2>Sobre a Agência</h2>
          <p>A  <strong> Imagem </strong> é uma agência focada em Qualidade e Resultado, com larga experiência no planejamento e implementação de Programas de construção, consolidação e reposicionamento da Marca e Imagem da sua Empresa.
        </div>

			  <div class="contato">
          <h2>Contato</h2>
          <span class="left col-xs-1 fa fa-map-marker"></span>
          <span class="right col-xs-11">Santa Rita do Sapucaí - MG</span>
          <div class="clear height10"></div>
          <span class="left col-xs-1 fa fa-envelope"></span>
          <span class="right col-xs-11">contato@imagemcomunicacao.com.br</span>
            <div class="clear height10"></div>
              <span class="left col-xs-1 fa fa-globe"></span>
              <span class="right col-xs-11">www.imagemcomunicacao.com.br</span>
            <div class="clear height10"></div>
           <span class="left col-xs-1 fa fa-skype"></span>
           <span class="right col-xs-11">agenciaimagem</span>
           <div class="clear height10"></div>
              <span class="left col-xs-1 fa fa-phone"></span>
              <span class="right col-xs-11">(35) 99191-9608</span>
            <div class="clear"></div>
        </div>
      </div>
    </div>
    <!--Radapé - Fim-->

	  <!-- Bottom Start -->
    <div class="templatemo_bottom">
    	<div class="container">
        	<div class="row">
            	<div class="left">Copyright © 2014 <a href="#">Imagem Comunicação e Marketing</a> </div>
                <div class="right">
                    <a href="#"><div class="fa fa-linkedin soc"></div></a>
                    <a href="#"><div class="fa fa-facebook soc"></div></a>
                </div>
            </div>
        </div>
    </div>
    <!-- Bottom End -->



    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <!-- <script src="https://code.jquery.com/jquery.js"></script> -->
	<script src="js/jquery-2.1.1.js"></script>
<script src="js/masonry.pkgd.min.js"></script>
<script src="js/jquery.flexslider-min.js"></script>
<script src="js/main.js"></script> <!-- Resource jQuery -->

    <script src="js/jquery-1.10.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.cycle2.min.js"></script>
    <script src="js/jquery.cycle2.carousel.min.js"></script>
    <script src="js/jquery.nivo.slider.pack.js"></script>
    <script>$.fn.cycle.defaults.autoSelector = '.slideshow';</script>
    <script type="text/javascript">
      $(function(){
          var default_view = 'grid';
          if($.cookie('view') !== 'undefined'){
              $.cookie('view', default_view, { expires: 7, path: '/' });
          }
          function get_grid(){
              $('.list').removeClass('list-active');
              $('.grid').addClass('grid-active');
              $('.prod-cnt').animate({opacity:0},function(){
                  $('.prod-cnt').removeClass('dbox-list');
                  $('.prod-cnt').addClass('dbox');
                  $('.prod-cnt').stop().animate({opacity:1});
              });
          }
          function get_list(){
              $('.grid').removeClass('grid-active');
              $('.list').addClass('list-active');
              $('.prod-cnt').animate({opacity:0},function(){
                  $('.prod-cnt').removeClass('dbox');
                  $('.prod-cnt').addClass('dbox-list');
                  $('.prod-cnt').stop().animate({opacity:1});
              });
          }
          if($.cookie('view') == 'list'){
              $('.grid').removeClass('grid-active');
              $('.list').addClass('list-active');
              $('.prod-cnt').animate({opacity:0});
              $('.prod-cnt').removeClass('dbox');
              $('.prod-cnt').addClass('dbox-list');
              $('.prod-cnt').stop().animate({opacity:1});
          }

          if($.cookie('view') == 'grid'){
              $('.list').removeClass('list-active');
              $('.grid').addClass('grid-active');
              $('.prod-cnt').animate({opacity:0});
                  $('.prod-cnt').removeClass('dboxlist');
                  $('.prod-cnt').addClass('dbox');
                  $('.prod-cnt').stop().animate({opacity:1});
          }

          $('#list').click(function(){
              $.cookie('view', 'list');
              get_list()
          });

          $('#grid').click(function(){
              $.cookie('view', 'grid');
              get_grid();
          });

          /* filter */
          $('.menuSwitch ul li').click(function(){
              var CategoryID = $(this).attr('category');
              $('.menuSwitch ul li').removeClass('cat-active');
              $(this).addClass('cat-active');

              $('.prod-cnt').each(function(){
                  if(($(this).hasClass(CategoryID)) == false){
                     $(this).css({'display':'none'});
                  };
              });
              $('.'+CategoryID).fadeIn();

          });
      });
    </script>
	<script src="js/jquery.singlePageNav.js"></script>

    <script type="text/javascript">
    $(window).load(function() {
        $('#slider').nivoSlider({
          prevText: '',
          nextText: '',
          controlNav: false,
        });
    });
    </script>
    <script>
      $(document).ready(function(){

        // hide #back-top first
        $("#back-top").hide();

        // fade in #back-top
        $(function () {
          $(window).scroll(function () {
            if ($(this).scrollTop() > 100) {
              $('#back-top').fadeIn();
            } else {
              $('#back-top').fadeOut();
            }
          });

          // scroll body to 0px on click
          $('#back-top a').click(function () {
            $('body,html').animate({
              scrollTop: 0
            }, 800);
            return false;
          });
        });

      });
      </script>
      <script type="text/javascript">
      <!--
          function toggle_visibility(id) {
             var e = document.getElementById(id);
             if(e.style.display == 'block'){
                e.style.display = 'none';
                $('#togg').text('show footer');
            }
             else {
                e.style.display = 'block';
                $('#togg').text('hide footer');
            }
          }
      //-->
      </script>
      <script type="text/javascript" src="js/lib/jquery.mousewheel-3.0.6.pack.js"></script>

      <script type="text/javascript">
      $(function() {
        $('a[href*=#]:not([href=#])').click(function() {
          if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
            if (target.length) {
              $('html,body').animate({
                scrollTop: target.offset().top
              }, 1000);
              return false;
            }
          }
        });
      });
      </script>
      <script src="js/stickUp.min.js" type="text/javascript"></script>
      <script type="text/javascript">
        //initiating jQuery
        jQuery(function($) {
          $(document).ready( function() {
            //enabling stickUp on the '.navbar-wrapper' class
            $('.mWrapper').stickUp();
          });
        });
      </script>
      <script>
     $('a.menu').click(function(){
    $('a.menu').removeClass("active");
    $(this).addClass("active");
	});
      </script>

      <script> <!-- scroll to specific id when click on menu -->
      	 // Cache selectors
var lastId,
    topMenu = $("#top-menu"),
    topMenuHeight = topMenu.outerHeight()+15,
    // All list items
    menuItems = topMenu.find("a"),
    // Anchors corresponding to menu items
    scrollItems = menuItems.map(function(){
      var item = $($(this).attr("href"));
      if (item.length) { return item; }
    });

// Bind click handler to menu items
// so we can get a fancy scroll animation
menuItems.click(function(e){
  var href = $(this).attr("href"),
      offsetTop = href === "#" ? 0 : $(href).offset().top-topMenuHeight+1;
  $('html, body').stop().animate({
      scrollTop: offsetTop
  }, 300);
  e.preventDefault();
});

// Bind to scroll
$(window).scroll(function(){
   // Get container scroll position
   var fromTop = $(this).scrollTop()+topMenuHeight;

   // Get id of current scroll item
   var cur = scrollItems.map(function(){
     if ($(this).offset().top < fromTop)
       return this;
   });
   // Get the id of the current element
   cur = cur[cur.length-1];
   var id = cur && cur.length ? cur[0].id : "";

   if (lastId !== id) {
       lastId = id;
       // Set/remove active class
       menuItems
         .parent().removeClass("active")
         .end().filter("[href=#"+id+"]").parent().addClass("active");
   }
});
      </script>

	 <script>
function slide(){

	var cycle_check, cycle_init, cycle_timer, cycle_next, cycle_active = 0;

(cycle_check = function() {
    var width = $(window).width(); // Checking size again after window resize
    if ( width < 768 && $( ".slideshow" ).attr( "data-cycle-carousel-visible" ) !== "2" ) {
        $( ".slideshow" ).cycle( "destroy" ).attr( "data-cycle-carousel-visible", "2" );
        cycle_init( 2 );
    } else if ( width > 768 && $( ".slideshow" ).attr( "data-cycle-carousel-visible" ) !== "4" ) {
        $( ".slideshow" ).cycle( "destroy" ).attr( "data-cycle-carousel-visible", "4" );
        cycle_init( 4 );
    }
})();

function cycle_init( visibleSlides ) {
    $( ".slideshow" ).cycle({
        fx: "carousel",
        speed: 800,
        slides: "> img",
        carouselVisible: visibleSlides,
        timeout: 2000,
        startingSlide: cycle_active
    });
}

$(window).resize( function() {
    clearTimeout( cycle_timer );
    cycle_timer = setTimeout( function() {
        cycle_check();
    }, 100 );
});

$(".slideshow").on("cycle-update-view", function ( e, optionHash, slideOptionsHash, currSlideEl ) {
    cycle_active = optionHash.currSlide;
});}
</script>

<script>
//<![CDATA[
$(window).load(function(){
$(document).ready(function () {
    var elements = document.getElementsByTagName("INPUT");
    for (var i = 0; i < elements.length; i++) {
        elements[i].oninvalid = function (e) {
            e.target.setCustomValidity("");
            if (!e.target.validity.valid) {
                switch (e.srcElement.id) {
                    case "nome":
                        e.target.setCustomValidity("Usuario");
                        break;
                    case "password":
                        e.target.setCustomValidity("Password cannot be blank");
                        break;
                }
            }
        };
        elements[i].oninput = function (e) {
            e.target.setCustomValidity("");
        };
    }
})
});//]]>

</script>
<!-- 	<input type="button" class="voltarTopo" onclick="$j('html,body').animate({scrollTop: $j('#voltarTopo').offset().top}, 2000);" value="Voltar ao topo" > -->
</body>
</html>
