<ul class="widget__follow-list">
                    <li><a target="_blank" class="item is-facebook" href="https://www.facebook.com/kayturepage"><span class="icon">Facebook</span> <span class="name">Facebook</span></a></li>
                    <li><a target="_blank" class="item is-twitter" href="https://twitter.com/Kayture"><span class="icon">Twitter</span> <span class="name">Twitter</span></a></li>
                    <li><a target="_blank" class="item is-instagram" href="http://instagram.com/kristina_bazan"><span class="icon">Instagram</span> <span class="name">Instagram</span></a></li>
                    <li><a target="_blank" class="item is-google" href="https://plus.google.com/+kayture/posts"><span class="icon">Google+</span> <span class="name">Google+</span></a></li>
                    <li><a target="_blank" class="item is-blogovin" href="http://www.bloglovin.com/blog/3879744/kayture"><span class="icon">Bloglovin</span> <span class="name">Bloglovin</span></a></li>
                    <li><a target="_blank" class="item is-youtube" href="http://www.youtube.com/user/KristinaBazan"><span class="icon">Youtube</span> <span class="name">Youtube</span></a></li>
                </ul>